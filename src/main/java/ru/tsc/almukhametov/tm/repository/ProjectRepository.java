package ru.tsc.almukhametov.tm.repository;

import ru.tsc.almukhametov.tm.api.repository.IProjectRepository;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public void add(Project project) {
        list.add(project);
    }

    @Override
    public void remove(Project project) {
        list.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Project findById(final String id) {
        for (Project project : list) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String name) {
        for (Project project : list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public boolean existById(final String id) {
        final Project project = findById(id);
        return project != null;
    }

    @Override
    public boolean existByIndex(final int index) {
        if (index < 0) return false;
        return index < list.size();
    }
}
