package ru.tsc.almukhametov.tm.api.repository;

import ru.tsc.almukhametov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(int index);

    boolean existById(String id);

    boolean existByIndex(int index);

}
