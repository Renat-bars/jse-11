package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    boolean existById(String id);

    boolean existByIndex(int index);
}
