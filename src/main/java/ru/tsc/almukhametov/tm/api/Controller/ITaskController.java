package ru.tsc.almukhametov.tm.api.Controller;

public interface ITaskController {

    void clearTasks();

    void createTasks();

    void showList();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();
}
